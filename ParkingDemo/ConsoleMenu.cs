﻿using System;
using ParkingSystem.Abstractions;

namespace ParkingDemo
{
    public class ConsoleMenu
    {
        private Controller _controller;
        bool exit;

        public ConsoleMenu()
        {
            this._controller = new Controller();
        }

        private void WriteLine(string text, ConsoleColor consoleColor = ConsoleColor.Blue)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public void Run()
        {
            while (!exit)
            {
                ShowMenuItems();
                int userInput = GetInput();
                Perform(userInput);
            }
        }

        private void Perform(int userInput)
        {
            switch (userInput)
            {
                case 0:
                    DisplayParkingBalance();
                    break;
                case 1:
                    DisplayLastMinuteEarnings();
                    break;
                case 2:
                    DisplayFreeAndOccupiedPlacesAmount();
                    break;
                case 3:
                    DisplayLastMinuteTransactions();
                    break;
                case 4:
                    DisplayAllTransaction();
                    break;
                case 5:
                    DisplayVehicleList();
                    break;
                case 6:
                    AddVehicle();
                    break;
                case 7:
                    RemoveVehicle();
                    break;
                case 8:
                    TopUpVehicleBalance();
                    break;
                case 9:
                    Exit();
                    break;

                default:
                    WriteLine("Please, Enter the number from 0 to 9.", ConsoleColor.Red);
                    break;
            }

            Console.WriteLine("Press enter");
            Console.ReadLine();
        }


        private int GetInput()
        {
            int userInput;
            do
            {
                WriteLine("Please, Enter your choice as a number:");
            } while (!int.TryParse(Console.ReadLine(), out userInput));

            return userInput;
        }

        private void ShowMenuItems()
        {
            Console.Clear();

            WriteLine("Choose option");

            Console.WriteLine();

            WriteLine("0 - Display parking balance");
            WriteLine("1 - Display the amount of money earned in the last minute");
            WriteLine("2 - Display the number of free and busy places at the parking");
            WriteLine("3 - Display parking transactions in the last minute");
            WriteLine("4 - Display the transaction history");
            WriteLine("5 - Display the list of all vehicles");
            WriteLine("6 - Park the vehicle");
            WriteLine("7 - Remove the vehicle");
            WriteLine("8 - Top up the balance of the vehicle");
            WriteLine("9 - Exit");

            Console.WriteLine();
        }

        private void DisplayParkingBalance()
        {
            WriteLine("Parking balance:");

            decimal parkingBalance = this._controller.GetParkingBalance();
            WriteLine(parkingBalance.ToString());
        }

        private void DisplayLastMinuteEarnings()
        {
            WriteLine("Money earned in last minute:");
            WriteLine(this._controller.GetLastMinuteEarnedMoney().ToString());
        }
        private void DisplayFreeAndOccupiedPlacesAmount()
        {
            var result = this._controller.GetFreeAndOccupiedPlacesAmount();
            WriteLine($"Amount of free places = { result.freePlaces}");
            WriteLine($"Amount of occupied places = { result.occupiedPlaces}");
        }

        private void DisplayLastMinuteTransactions()
        {
            WriteLine("Last transactions:");
            foreach (string transaction in this._controller.GetLastTransactions())
            {
                WriteLine(transaction);
            }
        }

        private void DisplayAllTransaction()
        {
            WriteLine("All transactions:");
            foreach (string transaction in this._controller.GetAllTransactions())
            {
                WriteLine(transaction);
            }

        }

        private void DisplayVehicleList()
        {
            WriteLine("Vehicles at parking:");

            foreach (Vehicle vehicle in this._controller.GetVehicleList())
            {
                WriteLine(vehicle.ToString());
            }
        }

        private void AddVehicle()
        {
            WriteLine("Vehicle types:");
            ShowVehicleType();
            Console.WriteLine();

            int vehicleTypeCode = -1;
            do
            {
                WriteLine("Enter vehicle type as a number:");
            } while (!int.TryParse(Console.ReadLine(), out vehicleTypeCode));

            PrintActionResult(this._controller.AddVehicle(vehicleTypeCode));

        }
        private void ShowVehicleType()
        {
            WriteLine("0 - Car");
            WriteLine("1 - Truck");
            WriteLine("2 - Bus");
            WriteLine("3 - Motorcycle");
        }

        private void RemoveVehicle()
        {
            int vehicleId;
            do
            {
                WriteLine("Enter vehicle id:");
            } while (!int.TryParse(Console.ReadLine(), out vehicleId));

            PrintActionResult(this._controller.RemoveVehicle(vehicleId));
        }

        private void TopUpVehicleBalance()
        {
            int vehicleId;
            do
            {
                Console.WriteLine("Enter vehicle id:");
            } while (!int.TryParse(Console.ReadLine(), out vehicleId));

            decimal moneyAmount;
            do
            {
                Console.WriteLine("Enter money amount:");
            } while (!decimal.TryParse(Console.ReadLine(), out moneyAmount));

            PrintActionResult(this._controller.TopUpVehicleBalance(vehicleId, moneyAmount));
        }

        private void PrintActionResult((bool isSuccessful, string message) result)
        {
            if (!result.isSuccessful)
            {
                WriteLine(result.message, ConsoleColor.Red);
            }
            else
            {
                WriteLine(result.message, ConsoleColor.Green);
            }
        }

        private void Exit()
        {
            exit = true;
        }
    }
}