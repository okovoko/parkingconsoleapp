﻿using System.Collections.Generic;
using ParkingSystem.Abstractions;
using ParkingSystem.Exceptions;
using ParkingSystem.Models;

namespace ParkingDemo
{
    public class Controller
    {

        private Parking _parking;

        public Controller()
        {
            this._parking = Parking.Instance;
        }

        public decimal GetParkingBalance()
        {
            return this._parking.Balance;
        }

        public decimal GetLastMinuteEarnedMoney()
        {
            return this._parking.TransactionService.Earnings();
        }

        public (int freePlaces, int occupiedPlaces) GetFreeAndOccupiedPlacesAmount()
        {
            int occupiedPlaces = this._parking.VehicleList.Count;
            int freePlaces = this._parking.Capacity - occupiedPlaces;
            return (freePlaces, occupiedPlaces);
        }

        public List<string> GetLastTransactions()
        {
            List<string> transactionsInfo = new List<string>();
            if (this._parking.TransactionService.Transactions == null)
            {
                transactionsInfo.Add("There is no transactions yet.");
            }
            else
            {
                foreach (Transaction t in this._parking.TransactionService.Transactions)
                {
                    transactionsInfo.Add(t.ToString());
                }
            }

            return transactionsInfo;
        }

        public string[] GetAllTransactions()
        {
            return this._parking.TransactionService.GetAllTransactions();
        }

        public List<Vehicle> GetVehicleList()
        {
            return this._parking.VehicleList;
        }

        public (bool, string) AddVehicle(int vehicleTypeCode)
        {
            try
            {
                this._parking.AddVehicle(VehicleFactory.CreateVehicle(vehicleTypeCode));
                return (true, $"Vehicle with type code { vehicleTypeCode} is parked.");
            }
            catch (NotEnoughPlacesException exc)
            {
                return (false, exc.Message);
            }
            catch (InvalidVehicleTypeCodeException exc)
            {
                return (false, exc.Message);
            }
        }

        public (bool, string) RemoveVehicle(int vehicleId)
        {
            try
            {
                this._parking.RemoveVehicle(vehicleId);
                return (true, $"Vehicle with id { vehicleId} is removed.");
            }
            catch (InvalidVehicleIdException exc)
            {
                return (false, exc.Message);
            }
        }

        public (bool, string) TopUpVehicleBalance(int vehicleId, decimal moneyAmount)
        {
            string message = "Vehicle balance is successfully topped up.";

            try
            {
                this._parking.GetVehicleById(vehicleId).TopUpBalance(moneyAmount);
            }
            catch (InvalidVehicleIdException exc)
            {
                return (false, exc.Message);
            }


            return (true, message);
        }
    }
}