﻿using System;
using System.Collections.Generic;
using System.Timers;
using ParkingSystem.Abstractions;
using ParkingSystem.Exceptions;
using ParkingSystem.Services;

namespace ParkingSystem.Models
{
    public sealed class Parking : IDisposable
    {
        private static readonly Lazy<Parking> _instance = new Lazy<Parking>(() => new Parking());

        private decimal _balance;
        private int _capacity;
        private Dictionary<string, decimal> _vehicleTypeFee;
        private List<Vehicle> _vehicleList;
        private TimeService _timeService;
        private TransactionService _transactionService;

        public decimal Balance { get { return this._balance; } }
        public int Capacity { get { return this._capacity; } }
        public Dictionary<string, decimal> VehicleTypeFee { get { return this._vehicleTypeFee; } }
        public List<Vehicle> VehicleList { get { return this._vehicleList; } }
        public TransactionService TransactionService { get { return this._transactionService; } }


        Parking()
        {
            this._balance = Settings.DefaultParkingBalance;
            this._capacity = Settings.ParkingCapacity;
            this._vehicleTypeFee = Settings.Prices;
            this._vehicleList = new List<Vehicle>();

            this._transactionService = new TransactionService();
            InitializeTimeService();
        }

        public static Parking Instance { get { return _instance.Value; } }

        public void AddVehicle(Vehicle vehicle)
        {
            if (this._capacity <= this._vehicleList.Count)
                throw new NotEnoughPlacesException("Parking is full. You cannot add another vehicle.");
            this._vehicleList.Add(vehicle);
        }

        public int VehicleIndex(int vehicleId) =>
            VehicleList.FindIndex(v => v.Id == vehicleId);

        public void RemoveVehicle(int vehicleId)
        {
            int vehicleIndex = VehicleIndex(vehicleId);
            if (vehicleIndex != -1)
            {
                this._vehicleList.RemoveAt(vehicleIndex);
            }
            else
            {
                throw new InvalidVehicleIdException("Invalid vehicle Id.");
            }
        }

        public Vehicle GetVehicleById(int vehicleId)
        {
            int vehicleIndex = VehicleIndex(vehicleId);
            if (vehicleIndex != -1)
            {
                return this._vehicleList[vehicleIndex];
            }
            else
            {
                throw new InvalidVehicleIdException("Invalid vehicle Id.");
            }
        }

        private void InitializeTimeService()
        {
            Dictionary<ElapsedEventHandler, int> Events = new Dictionary<ElapsedEventHandler, int>();

            Events.Add((obj, eArgs) =>
            {
                this._transactionService.WriteToFile();
                this._transactionService.ClearHistory();
            },
            Settings.LogPeriod);

            Events.Add((obj, eArgs) =>
            {
                foreach (Vehicle vehicle in this._vehicleList)
                {
                    decimal price = this._vehicleTypeFee[vehicle.GetType().Name];
                    decimal fee = vehicle.Balance < price ? price * Settings.FeePenaltyRatio : price;

                    vehicle.WithdrawBalance(fee);
                    this._balance += fee;

                    this._transactionService.Add(new Transaction(vehicle.Id, fee));
                }
            },
            Settings.PaymentPeriod);

            this._timeService = new TimeService(Events);
        }

        public void Dispose()
        {
            this._timeService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}