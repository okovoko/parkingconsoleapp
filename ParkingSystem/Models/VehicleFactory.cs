﻿using ParkingSystem.Abstractions;
using ParkingSystem.Exceptions;

namespace ParkingSystem.Models
{
    public static class VehicleFactory
    {
        public static readonly int MaxVehicleCode = 3;
        public static Vehicle CreateVehicle(int vehicleCode)
        {
            switch (vehicleCode)
            {
                case 0:
                    return new Car();
                case 1:
                    return new Truck();
                case 2:
                    return new Bus();
                case 3:
                    return new Motorcycle();
                default:
                    throw new InvalidVehicleTypeCodeException($"There is no such vehicle type with code {vehicleCode}");
            }
        }
    }
}