﻿using System;
namespace ParkingSystem.Exceptions
{
    public class InvalidVehicleTypeCodeException : Exception
    {
        public InvalidVehicleTypeCodeException()
        {
        }

        public InvalidVehicleTypeCodeException(string message)
            : base(message)
        {
        }

        public InvalidVehicleTypeCodeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}