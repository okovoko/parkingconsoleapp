﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace ParkingSystem.Services
{
    public class TimeService : IDisposable
    {
        private List<Timer> _timers;
        private bool disposed = false;

        public TimeService(Dictionary<ElapsedEventHandler, int> Events)
        {
            this._timers = new List<Timer>();
            InitializeTimers(Events);
        }

        private void InitializeTimers(Dictionary<ElapsedEventHandler, int> Events)
        {
            foreach (var eventPair in Events)
            {
                Timer newTimer = new Timer(eventPair.Value);
                newTimer.Elapsed += eventPair.Key;
                newTimer.AutoReset = true;
                newTimer.Enabled = true;
                this._timers.Add(newTimer);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    foreach (Timer timer in this._timers)
                    {
                        timer.Stop();
                        timer.Dispose();
                    }
                }
            }
            disposed = true;
        }
    }
}
