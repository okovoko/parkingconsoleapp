﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ParkingSystem.Models;

namespace ParkingSystem.Services
{
    public class TransactionService
    {
        private List<Transaction> _transactions;
        public string TransactionLogFile { get; set; }

        public List<Transaction> Transactions { get { return this._transactions; } }

        public TransactionService()
        {
            this._transactions = new List<Transaction>();
            this.TransactionLogFile = Settings.TransactionLogFile;
        }

        public void Add(Transaction transaction)
        {
            this._transactions.Add(transaction);
        }

        public void ClearHistory()
        {
            this._transactions.Clear();
        }

        public void WriteToFile()
        {
            using (var tw = new StreamWriter(this.TransactionLogFile, true))
            {
                foreach (Transaction transaction in this._transactions)
                {
                    tw.WriteLine(transaction.ToString());
                }
            }
        }

        public decimal Earnings()
        {
            return this._transactions.Sum(t => t.MoneyAmount);
        }

        public string[] GetAllTransactions()
        {
            try
            {
                return File.ReadAllLines(this.TransactionLogFile);
            }
            catch (FileNotFoundException)
            {
                return new string[] {"There are no transactions yet. (Transactions.log doesn't exist)"};
            }
        }
    }
}