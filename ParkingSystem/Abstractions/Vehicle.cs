﻿using System;

namespace ParkingSystem.Abstractions
{
    abstract public class Vehicle
    {
        static int LastId = 0;

        private decimal _balance;

        public int Id { get; }
        public decimal Balance { get { return this._balance; } }
        public DateTime ParkingStartTime { get; }

        public Vehicle()
            : this(Settings.DefaultVehicleBalance)
        {
        }

        public Vehicle(decimal balance)
        {
            this.Id = ++LastId;
            this._balance = balance;
            this.ParkingStartTime = DateTime.Now;
        }

        public void TopUpBalance(decimal moneyAmount)
        {
            this._balance += moneyAmount;
        }

        public void WithdrawBalance(decimal moneyAmount)
        {
            this._balance -= moneyAmount;
        }

        public override string ToString()
        {
            return $"Id: {this.Id}\tType: {this.GetType().Name}\tBalance: {this._balance}";
        }
    }
}